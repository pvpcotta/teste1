#!/usr/bin/env python
# coding: utf-8

import keras
from keras.layers import Dense, Conv2D, MaxPool2D
from keras.layers import Flatten, BatchNormalization
from keras.layers import Dropout
from keras.models import Sequential


def cnn_model(num_input, num_output):
    
	model = Sequential()

	model.add(Conv2D(filters=32, kernel_size=[3,3], strides=1, activation="tanh", padding="SAME", input_shape=num_input))
	model.add(Conv2D(filters=32, kernel_size=[3,3], strides=1, activation="tanh", padding="SAME"))
	model.add(MaxPool2D(pool_size=[2,2], padding="SAME"))

	model.add(Conv2D(filters=64, kernel_size=[3,3], strides=1, activation="tanh", padding="SAME"))
	model.add(Conv2D(filters=64, kernel_size=[3,3], strides=1, activation="tanh", padding="SAME"))
	model.add(MaxPool2D(pool_size=[2,2], padding="SAME"))

	model.add(Conv2D(filters=128, kernel_size=[3,3], strides=1, activation="tanh", padding="SAME"))
	model.add(Conv2D(filters=128, kernel_size=[3,3], strides=1, activation="tanh", padding="SAME"))
	model.add(MaxPool2D(pool_size=[2,2], padding="SAME"))

	model.add(Conv2D(filters=256, kernel_size=[3,3], strides=1, activation="tanh", padding="SAME"))
	model.add(Conv2D(filters=256, kernel_size=[3,3], strides=1, activation="tanh", padding="SAME"))
	model.add(MaxPool2D(pool_size=[2,2], padding="SAME"))

	model.add(Flatten())

	model.add(Dense(512, activation="relu"))
	model.add(Dropout(0.1))
	model.add(Dense(num_output, activation="softmax"))

	SGD = keras.optimizers.SGD(lr=1e-3, momentum=1e-5)
	model.compile(optimizer=SGD, loss='binary_crossentropy', metrics=['accuracy'])

	return model

def get_response(output):
	
	classe_label = ["CNH", "RG", "RNE"]
	print(output.argmax())
	return classe_label[output.argmax()]
