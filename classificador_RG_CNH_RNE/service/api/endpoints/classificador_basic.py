import logging

from flask import Flask
from flask import request
from flask_restplus import Resource, Api 
from api.restplus import api
from flask import jsonify
from ..bo.train import classifier_frente, classifier_verso
from ..utils.base64_utils import decode_image_from_base64
from ..utils.Model import get_response
log = logging.getLogger(__name__)

ns = api.namespace('endpoints/classificador', description='Post operação.')

@ns.route('/frente')
class PostsCollection(Resource):

	@api.response ( 200, 'Enviado com sucesso.' )
	def post (self):
		'''
		Método POST para classificação da frente dos documentos RG e CNH
		'''
		response = {}
		request_data = request.get_json()
		b64_image = request_data["image"]
		image = decode_image_from_base64(b64_image)

		response["doc"] = get_response(classifier_frente(image))
		response = jsonify(response)

		return response

@ns.route('/verso')
class PostsCollection(Resource):

	@api.response ( 200, 'Enviado com sucesso.' )
	def post (self):
		'''
		Método POST para classificação do verso dos documentos RG e CNH
		'''
		response = {}
		request_data = request.get_json()
		b64_image = request_data["image"]
		image = decode_image_from_base64(b64_image)

		response["doc"] = get_response(classifier_verso(image))
		response = jsonify(response)

		return response
