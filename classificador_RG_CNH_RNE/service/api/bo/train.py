#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import numpy as np
import cv2
import tensorflow as tf
from ..utils.Model import cnn_model

import keras
from sklearn.model_selection import train_test_split

dir_inter = "/media/stefanini/Windows/dataset/POC_inter/"
path_resized_frente = dir_inter + "resized/resized_frente/"
path_resized_verso = dir_inter + "resized/resized_verso/"
path_model_frente = dir_inter + "model CNN/Frente/"
path_model_verso = dir_inter + "model CNN/Verso/"


model_weight_frente = "./model_weights/frente-20-0.97.hdf5"
model_weight_verso = dir_inter + "./model_weights/verso-45-0.91.hdf5"
classe_label = ["RG", "CNH", "RNE"]

def train_frente(epochs, batch_size, path_resized_frente):
	keras.backend.clear_session()

	features_frente, label_frente = get_images(path_resized_frente)
	label_frente = pd.get_dummies(label_frente, drop_first=True).values
	label_frente = np.reshape(label_frente, (label_frente.shape[0],))

	X_train, X_test, y_train, y_test = train_test_split(features_frente, label_frente, test_size=0.15)

	num_input = features_frente.shape[1:]
	num_output = len(classe_label)

	model_frente = cnn_model(num_input, num_output)

	filepath_frente =  path_model_frente + "frente-{epoch:02d}-{val_acc:.2f}.hdf5"
	callback_save = ModelCheckpoint(filepath_frente, monitor="val_acc", save_best_only=True, save_weights_only=False)
	callback_early_stopping = EarlyStopping(monitor='loss', patience=5)

	history = model_frente.fit(X_train,
		            y_train,
		            epochs=epochs,
		            batch_size=batch_size,
		            callbacks = [callback_save, callback_early_stopping],
		            validation_data=[X_test, y_test], verbose=1)
	return history

def train_verso(epochs, batch_size, path_resized_verso):
	keras.backend.clear_session()

	features_verso, label_verso = get_images(path_resized_verso)
	label_verso = pd.get_dummies(label_verso, drop_first=True).values
	label_verso = np.reshape(label_verso, (label_verso.shape[0],))

	X_train, X_test, y_train, y_test = train_test_split(features_verso, label_verso, test_size=0.15)

	num_input = features_verso.shape[1:]
	num_output = len(classe_label)

	model_verso = cnn_model(num_input, num_output)


	filepath_verso =  path_model_verso + "verso-{epoch:02d}-{val_acc:.2f}.hdf5"
	callback_save = ModelCheckpoint(filepath_verso, monitor="val_acc", save_best_only=True, save_weights_only=False)
	callback_early_stopping = EarlyStopping(monitor='loss', patience=5)

	history = model_verso.fit(X_train,
		            y_train,
		            epochs=epochs,
		            batch_size=batch_size,
		            callbacks = [callback_save, callback_early_stopping],
		            validation_data=[X_test, y_test], verbose=1)

	return history

def classifier_frente(image):
	keras.backend.clear_session()

	image = resize_image(image)
	num_input = image.shape
	num_output = len(classe_label)
	img = np.reshape(image, (1, image.shape[0], image.shape[1], image.shape[2]))

	model_classifier_frente = cnn_model(num_input, num_output)
	model_frente = keras.models.load_model(model_weight_frente)
	predict = model_frente.predict(img)
	return(predict)
	

def classifier_verso(image):
	keras.backend.clear_session()

	image = resize_image(image)
	num_input = image.shape
	num_output = len(classe_label)
	img = np.reshape(image, (1, image.shape[0], image.shape[1], image.shape[2]))

	model_classifier_frente = cnn_model(num_input, num_output)
	model_verso = keras.models.load_model(model_weight_verso)
	predict = model_verso.predict(img)
	return(predict)


def resize_image(image):
    return cv2.resize(image, (400, 350))
	
